 <?php
function mmmr($array, $output = 'mean'){
	if(!is_array($array)){
		return NULL;	
	}
   
	switch($output){
		case 'mean':
			$count = count($array);
			$sum = array_sum($array);
			$result = $sum / $count;
		break;
		case 'median':
			rsort($array);
			$middle = round(count($array) / 2);
			$result = $array[$middle-1];
		break;
		case 'mode':
			$v = array_count_values($array);
			arsort($v);
			foreach($v as $k => $v){
				$result = $k; 
				break;
			}
		break;
		case 'range':
			sort($array);
			$small = $array[0];
			rsort($array);
			$large = $array[0];
			$result = $large - $small;
		break;
	}
	return round($result, 3);
}

if(!isset($_POST['numbers'])){
	$result["error"]["code"] = 404;
	$result["error"]["message"] = "Method GET not available on this endpoint";
}
else{
	$array = $_POST['numbers'];
	$result["results"]["mean"] = mmmr($array);
	$result["results"]["median"] = mmmr($array, 'median');
	$result["results"]["mode"] = mmmr($array, 'mode');
	$result["results"]["range"] = mmmr($array, 'range');	
}
echo json_encode($result);
?> 