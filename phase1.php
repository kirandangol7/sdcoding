 <?php
function mmmr($array, $output = 'mean'){
	if(!is_array($array)){
		return NULL;	
	}
   
	switch($output){
		case 'mean':
			$count = count($array);
			$sum = array_sum($array);
			$result = $sum / $count;
		break;
		case 'median':
			rsort($array);
			$middle = round(count($array) / 2);
			$result = $array[$middle-1];
		break;
		case 'mode':
			$v = array_count_values($array);
			arsort($v);
			foreach($v as $k => $v){
				$result = $k; 
				break;
			}
		break;
		case 'range':
			sort($array);
			$small = $array[0];
			rsort($array);
			$large = $array[0];
			$result = $large - $small;
		break;
	}
	return round($result, 3);
}

$array = array(13,23,12,44,55);

//calculate mean
echo 'Mean: '.mmmr($array).'<br>';

//calculate median
echo 'Median: '.mmmr($array, 'median').'<br>';

//calculate mode
echo 'Mode: '.mmmr($array, 'mode').'<br>';

//calculate range
echo 'Range: '.mmmr($array, 'range');
?> 