<?php
function callAPI($url, $data){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
	$result = curl_exec($ch);
	curl_close($ch); 
	return $result;
}

$data = array("numbers" => array(5, 6, 8, 7, 5));                                                                    
$url = "http://localhost/sdcoding/api/mmmr";
                                                         
echo callAPI($url, http_build_query($data));

//error json
echo "<br>";
$errorData = array("data" => array(5, 6, 8, 7, 5));
echo callAPI($url, http_build_query($errorData));
?>